# README #

### This README would normally document whatever steps are necessary to get your application up and running.

### Steps to start Webpack:
* 1. Install node modules in the config folder "yarn install" / "npm install".
* 2. Run webpack with "yarn watch" / "npm watch".

### Branch prefixes:
* bugfix/ --> bug fixes
* feature/ --> new features
* hotfix/ --> for urgent bug fixing
* release/ --> For testing

### Repo dupliceren
* Create repository
* Import repository
* fill in url: https://bitbucket.org/startmetplate/template-thema-repository/src/master/